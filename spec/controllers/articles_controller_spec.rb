require 'rails_helper'

RSpec.describe ArticlesController, type: :controller do
  describe "GET index" do
    it "shows articles" do
      article = Article.create({title: 'My article', text: 'My very detailed article'})
      get :index

      expect(assigns(:articles)).to eq([article])
    end
  end
end
